# Synopsis

Parses the list of Debian-mirrors downloaded from ftp.debian.org and then based on the region specified by the user each of testes mirrors for bandwidth, at the end it prints the mirror with the highest bandwidth.

# Installation

At the moment this project is not packaged for debian (yet), so the only way you can get it is with a git clone

## Dependencies so far:

* python humanize
* python incf.countryutils

You can get them with:

```sh
$ apt-get install python-pip python-humanize
$ pip install incf.countryutils
```

# Howto use it

```sh
$ apt-spy -c DE
 Benchmarking 33 debian-mirrors: 
 Mirrorname                     | FTP Speed | HTTP Speed 
 ftp.de.debian.org              | 2.4 MB/s  | 1.3 MB/s   
 ftp2.de.debian.org             | 1.7 MB/s  | 1.6 MB/s   
 ftp.tu-clausthal.de            | 1.6 MB/s  | 685.7 kB/s 
 debian.uni-duisburg-essen.de   | 1.6 MB/s  | 1.7 MB/s  
 ...
 Mirror with highest throughput: ftp.de.debian.org
```

Now apt-spy will generate a new apt-spy.list in the directory(/etc/apt/sources.list.d/) at first time!The content will be the fastest mirror written in the format of Debian list file! 

# Contributors

* mu dongliang

# License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

