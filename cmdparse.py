from __future__ import print_function
import argparse
import requests
from incf.countryutils import transformations
from incf.countryutils.datatypes import Country, Continent
from incf.countryutils.data import ctca2_to_ctn
import sys
from mirror_parse import all_countries
import os
import subprocess

etc_file = "/etc/apt/sources.list.d/apt-spy.list"

parser = argparse.ArgumentParser(prog="apt-spy")
parser.add_argument("-a", dest="area", help="Area to benchmark (ie, Europe).")
parser.add_argument("-d", dest="distribution", help="Debian distribution (ie, stable).")
parser.add_argument("-l", "--list-countries", help="List all countries with a debian mirror for reference", action="store_true")
parser.add_argument("--list-continents", help="List all continents for users to choose", action="store_true")
parser.add_argument("--ftp-only", help="Only check FTP mirrors (ignore HTTP)", action="store_true")
parser.add_argument("--http-only", help="Only check HTTP mirrors (ignore FTP)", action="store_true")
parser.add_argument("-c", "--countries", dest="countries", help="Comma separated Countries to benchmark (ie. US).", type=str)
parser.add_argument("-u", "--update-mirror", dest="update_mirror", help="Update local mirror-file from http://ftp.debian.org/debian/.", action="store_true")
parser.add_argument("-U", "--update-mirror-url", dest="mirror_destination", help="URL to update the local mirror-file from.", type=str)
parser.add_argument("-f", dest="target_file", help="File to grab when benchmarking. Relative to Debian base. (Default is ls-lR.gz)")
parser.add_argument("-w", "--write-config", help="Write fastest apt-spy mirror to apt.conf", action="store_true")

args = parser.parse_args()

# No parameters show help
if len(sys.argv) == 1:
    parser.print_help()
    exit(1)

if args.area:
    countries = []
    if args.area not in ctca2_to_ctn.keys():
        print("Error: Unknown continent code: " + args.area)
        exit(0)
    else:
        continent = Continent(args.area)
        for ccountry in continent.countries:
            if ccountry.alpha2 in all_countries:
                countries.append(ccountry.alpha2)   # only list country name here
else:
    countries = all_countries

# Write-config checks
if args.write_config:
    write_config = args.write_config
    if (os.access(etc_file, os.F_OK) and not os.access(etc_file, os.W_OK)) or (not os.access(etc_file, os.F_OK) and not os.access(os.path.dirname(etc_file), os.W_OK)):
        print("Error: Missing write permission to " + str(etc_file) + ". Check permissions or run as root.")
        exit(1)

# Accept the distribution supplied by the user
if args.distribution:
    distribution = args.distribution
else :
    distribution = subprocess.check_output(["lsb_release", "-c"]).strip().split("\t")[1];

# List continents if requested
if args.list_continents:
    print("Available Continents are:")
    for continent, name in ctca2_to_ctn.items():
        print("- " + continent + ": " + name)
    exit(0)

# List countries if requested
if args.list_countries:
    print("Available countries are:")
    for country in all_countries:
        print("- " + country + ": " + transformations.cc_to_cn(country) + "("+Country(country).continent.name+")")
    exit(0)

# Parse countries supplied by the user
if args.countries:
    countries = args.countries.split(",")
    for country in countries:
        if country.upper() not in all_countries:
            print("Error: Unknown countrycode: " + country + " (It is not on the debian mirrors file).")
            exit(1)
else:
    countries = all_countries

# Update the mirrors-file
mirror_destination = "http://ftp.debian.org/debian/README.mirrors.txt"
local_mirror_file = "README.mirrors.txt"
if args.update_mirror:
    print ("Updating Mirrors file: ", end='')
    if args.mirror_destination:
        mirror_destination = args.mirror_destination
    try:
        response = requests.get(mirror_destination, timeout=4)
        data = response.content
    except (requests.ConnectionError, requests.Timeout):
        print("ERROR: Could not get " + mirror_destination)
        exit(1)
    try:
        with open(local_mirror_file, "w") as local_mirror_filedesc:
            local_mirror_filedesc.write(data)
        print ("OK")
    except IOError:
        print("Error: Could not write to " + str(local_mirror_file))
        exit(1)

    # If no other parameters specified, exit after update
    if len(sys.argv) == 2:
        exit(0)

# Target file
if args.target_file:
    target_file = args.target_file
else:
    target_file = "ls-lR.gz"

# Enabling/DisablingFTP/HTTP (Default = Both)
ftp = http = True
if args.http_only:
    ftp = False
if args.ftp_only:
    http = False
